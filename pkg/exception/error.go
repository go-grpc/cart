package exception

import (
	"errors"
)

type ServerError struct {
	err error
}

func (s ServerError) Error() string {
	return s.err.Error()
}

func New(msg string) error {
	return &ServerError{err: errors.New(msg)}
}

var notFound = New("не найден")
var exist = New("уже существует")
var decode = New("decode")

func NotFoundException() error {
	return notFound
}

func InternalError() error {
	return New("что то пошло не так")
}

func ExistException() error {
	return exist
}
func DecodeException() error {
	return decode
}
