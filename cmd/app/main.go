package main

import (
	"bus/cart/internal/app/pkg/app"
	"log"
)

func main() {
	a, err := app.New()
	if err != nil {
		log.Fatal(err)
	}
	if err = a.Run(); err != nil {
		log.Fatal(err)
	}
}
