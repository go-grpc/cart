CREATE TABLE IF NOT EXISTS cart(
    id serial primary key,
    user_id int unique not null
);

CREATE TABLE IF NOT EXISTS cart_items(
    id serial primary key,
    product_id varchar(155) not null ,
    qty int not null ,
    cart_id int,
    CONSTRAINT cart_fk FOREIGN KEY (cart_id) REFERENCES cart(id)
);

SELECT cart_items.id as id, product_id, qty, cart_id
FROM cart_items
LEFT JOIN cart c on cart_items.cart_id = c.id