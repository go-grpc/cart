// Code generated by mockery v2.15.0. DO NOT EDIT.

package mocks

import (
	domain "bus/cart/internal/app/domain/model"
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// Dao is an autogenerated mock type for the Dao type
type Dao struct {
	mock.Mock
}

// CheckCart provides a mock function with given fields: ctx, userId
func (_m *Dao) CheckCart(ctx context.Context, userId uint32) (uint32, error) {
	ret := _m.Called(ctx, userId)

	var r0 uint32
	if rf, ok := ret.Get(0).(func(context.Context, uint32) uint32); ok {
		r0 = rf(ctx, userId)
	} else {
		r0 = ret.Get(0).(uint32)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint32) error); ok {
		r1 = rf(ctx, userId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateCart provides a mock function with given fields: ctx, userId
func (_m *Dao) CreateCart(ctx context.Context, userId uint32) (uint32, error) {
	ret := _m.Called(ctx, userId)

	var r0 uint32
	if rf, ok := ret.Get(0).(func(context.Context, uint32) uint32); ok {
		r0 = rf(ctx, userId)
	} else {
		r0 = ret.Get(0).(uint32)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint32) error); ok {
		r1 = rf(ctx, userId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CreateCartItem provides a mock function with given fields: ctx, item
func (_m *Dao) CreateCartItem(ctx context.Context, item domain.CartItem) error {
	ret := _m.Called(ctx, item)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, domain.CartItem) error); ok {
		r0 = rf(ctx, item)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: ctx, id
func (_m *Dao) Delete(ctx context.Context, id uint32) error {
	ret := _m.Called(ctx, id)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, uint32) error); ok {
		r0 = rf(ctx, id)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetCartItemById provides a mock function with given fields: ctx, itemId
func (_m *Dao) GetCartItemById(ctx context.Context, itemId uint32) (domain.CartItem, error) {
	ret := _m.Called(ctx, itemId)

	var r0 domain.CartItem
	if rf, ok := ret.Get(0).(func(context.Context, uint32) domain.CartItem); ok {
		r0 = rf(ctx, itemId)
	} else {
		r0 = ret.Get(0).(domain.CartItem)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint32) error); ok {
		r1 = rf(ctx, itemId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCartItemByProductId provides a mock function with given fields: ctx, productId
func (_m *Dao) GetCartItemByProductId(ctx context.Context, productId string) (domain.CartItem, error) {
	ret := _m.Called(ctx, productId)

	var r0 domain.CartItem
	if rf, ok := ret.Get(0).(func(context.Context, string) domain.CartItem); ok {
		r0 = rf(ctx, productId)
	} else {
		r0 = ret.Get(0).(domain.CartItem)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, productId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetCartItems provides a mock function with given fields: ctx, cartId
func (_m *Dao) GetCartItems(ctx context.Context, cartId uint32) ([]domain.CartItem, error) {
	ret := _m.Called(ctx, cartId)

	var r0 []domain.CartItem
	if rf, ok := ret.Get(0).(func(context.Context, uint32) []domain.CartItem); ok {
		r0 = rf(ctx, cartId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]domain.CartItem)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint32) error); ok {
		r1 = rf(ctx, cartId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateQty provides a mock function with given fields: ctx, id, qty
func (_m *Dao) UpdateQty(ctx context.Context, id uint32, qty uint32) error {
	ret := _m.Called(ctx, id, qty)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, uint32, uint32) error); ok {
		r0 = rf(ctx, id, qty)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewDao interface {
	mock.TestingT
	Cleanup(func())
}

// NewDao creates a new instance of Dao. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewDao(t mockConstructorTestingTNewDao) *Dao {
	mock := &Dao{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
