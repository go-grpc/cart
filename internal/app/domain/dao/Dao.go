package dao

import (
	"bus/cart/internal/app/domain"
	"bus/cart/internal/app/domain/model"
	"bus/cart/pkg/client/postgresql"
	"bus/cart/pkg/exception"
	"bus/cart/pkg/logger"
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
)

type dao struct {
	pg     postgresql.Client
	logger *logger.Logger
}

func (d dao) CreateCart(ctx context.Context, userId uint32) (uint32, error) {
	d.logger.Debug(fmt.Sprintf("creating cart for user %d", userId))
	q := `INSERT INTO cart (user_id) VALUES ($1) RETURNING id`
	var id uint32
	res, err := d.pg.Exec(ctx, q, userId)
	if err != nil || !res.Insert() {
		return 0, fmt.Errorf("ошибка в созданий карты")
	}
	d.logger.Info(fmt.Sprintf("created cart for user %d", userId))
	return id, nil
}

func (d dao) CreateCartItem(ctx context.Context, item model.CartItem) error {
	d.logger.Debug(fmt.Sprintf("creating cart item for cart %d", item.CartId))
	q := `INSERT INTO cart_items (product_id, qty, cart_id) VALUES ($1,$2,$3)`
	res, err := d.pg.Exec(ctx, q, item.ProductId, item.Qty, item.CartId)
	if err != nil || !res.Insert() {
		return fmt.Errorf("ошибка в созданий карты")
	}
	d.logger.Info(fmt.Sprintf("created item for cart %d", item.CartId))
	return nil

}

func (d dao) UpdateQty(ctx context.Context, id uint32, qty uint32) error {
	d.logger.Debug(fmt.Sprintf("updating cart qty by id:  %d", id))
	q := `UPDATE cart_items SET qty = $1 WHERE id = $2`
	res, err := d.pg.Exec(ctx, q, qty, id)
	if err != nil {
		return fmt.Errorf("ошибка в созданий карты")
	}
	if res.RowsAffected() != 1 {
		return fmt.Errorf("cart item: %d %w", id, exception.NotFoundException())
	}
	d.logger.Info(fmt.Sprintf("update item for cart %d", id))
	return nil

}

func (d dao) Delete(ctx context.Context, id uint32) error {
	d.logger.Debug(fmt.Sprintf("deleting cart item by id:  %d", id))
	q := `DELETE FROM cart_items WHERE id = $1`
	commandTag, err := d.pg.Exec(ctx, q, id)
	if err != nil {
		return fmt.Errorf("ошибка в удалений карты")
	}
	if commandTag.RowsAffected() != 1 {
		return exception.NotFoundException()
	}
	d.logger.Debug(fmt.Sprintf("delete cart item by id:  %d", id))
	return nil
}

func (d dao) GetCartItems(ctx context.Context, userId uint32) ([]model.CartItem, error) {
	d.logger.Debug(fmt.Sprintf("get cart itemm by id:  %d", userId))
	q := `SELECT cart_items.id as id, product_id, qty, cart_id
          FROM cart_items
          LEFT JOIN cart c on c.user_id = $1
         `
	rows, err := d.pg.Query(ctx, q, userId)

	if err != nil {
		return nil, fmt.Errorf("что то пошло не так")
	}
	defer rows.Close()
	result := make([]model.CartItem, 0, 10)
	item := model.CartItem{}
	for rows.Next() {
		if err = rows.Scan(&item.Id, &item.ProductId, &item.Qty, &item.CartId); err != nil {
			return nil, err
		}
		result = append(result, item)
	}
	return result, err

}
func (d dao) GetCartItemById(ctx context.Context, itemId uint32) (model.CartItem, error) {
	d.logger.Debug(fmt.Sprintf("geting one item by id: %d", itemId))

	q := `SELECT id,qty,product_id,cart_id FROM cart_items WHERE id = $1`
	res := model.CartItem{}
	err := d.pg.QueryRow(ctx, q, itemId).Scan(&res)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			d.logger.Error(fmt.Sprintf("not found  item by  id: %d", itemId))
			return res, fmt.Errorf("cart item %w", exception.NotFoundException())
		}
		return res, fmt.Errorf("%w", exception.InternalError())
	}
	d.logger.Debug(fmt.Sprintf("geting one cart by  item id: %d", itemId))
	return res, nil
}
func (d dao) CheckCart(ctx context.Context, userId uint32) (uint32, error) {
	d.logger.Debug(fmt.Sprintf("geting one cart by  item id: %d", userId))

	q := `SELECT id FROM cart WHERE user_id = $1`
	var id uint32
	rows, err := d.pg.Query(ctx, q, userId)
	defer rows.Close()
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			d.logger.Error(fmt.Sprintf("not found cart by  item id: %d", userId))
			return id, fmt.Errorf("cart item %w", exception.NotFoundException())
		}
		return id, fmt.Errorf("%w", exception.InternalError())
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			d.logger.Error(fmt.Sprintf("ошибка при decode cart id"))
			return 0, fmt.Errorf("cart item %w", exception.InternalError())
		}
	}
	d.logger.Debug(fmt.Sprintf("get one cart id by user_id: %d", id))
	return id, nil
}
func (d dao) GetCartItemByProductId(ctx context.Context, productId string) (model.CartItem, error) {
	d.logger.Debug(fmt.Sprintf("geting one item by productId: %s", productId))

	q := `SELECT id,qty,product_id,cart_id FROM cart_items WHERE id = $1`
	res := model.CartItem{}
	rows, err := d.pg.Query(ctx, q, productId)
	rows.Close()
	if err != nil {
		return res, fmt.Errorf("%w", exception.InternalError())
	}
	for rows.Next() {
		if err = rows.Scan(&res.Id, &res.Qty, &res.ProductId, &res.CartId); err != nil {
			return model.CartItem{}, fmt.Errorf("item %w", exception.DecodeException())
		}
	}
	d.logger.Debug(fmt.Sprintf("geting one item by  product id: %s", productId))
	return res, nil
}
func (d dao) CartItemToOrder(ctx context.Context, orderId uint32, itemsIds []uint32) error {
	d.logger.Debug(fmt.Sprintf("add cart items to order"))
	if len(itemsIds) == 0 {
		return nil
	}
	q := `UPDATE TABLE cart_items`
	for _, id := range itemsIds {
		q += fmt.Sprintf("order_id = %d WHERE id = %d", orderId, id)
	}
	res, err := d.pg.Exec(ctx, q)
	if err != nil {
		return fmt.Errorf("%w", exception.InternalError())
	}
	if !res.Update() {
		return exception.NotFoundException()
	}
	d.logger.Info("success end request")
	return nil

}
func (d dao) GetCartItemsByOrderId(ctx context.Context, orderId uint32) ([]model.CartItem, error) {
	d.logger.Debug(fmt.Sprintf("get cart items by order id"))
	q := `SELECT id,product_id,qty FROM cart_items WHERE order_id = $1`
	rows, err := d.pg.Query(ctx, q, orderId)
	if err != nil {

	}
	res := make([]model.CartItem, 0, 5)
	item := model.CartItem{}
	for rows.Next() {
		err := rows.Scan(&item.Id, &item.ProductId, &item.Qty)
		if err != nil {
			return nil, exception.DecodeException()
		}
		res = append(res, item)
	}
	d.logger.Info("success end request")
	return res, err
}
func New(r postgresql.Client, logger *logger.Logger) domain.Dao {
	return dao{pg: r, logger: logger}
}
