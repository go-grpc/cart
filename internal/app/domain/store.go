package domain

import (
	"bus/cart/internal/app/domain/model"
	"context"
)

//go:generate mockery --dir .  --name Dao --output dao/mocks
type Dao interface {
	CreateCart(ctx context.Context, userId uint32) (uint32, error)
	CheckCart(ctx context.Context, userId uint32) (uint32, error)
	CreateCartItem(ctx context.Context, item model.CartItem) error
	UpdateQty(ctx context.Context, id uint32, qty uint32) error
	Delete(ctx context.Context, id uint32) error
	GetCartItems(ctx context.Context, cartId uint32) ([]model.CartItem, error)
	GetCartItemById(ctx context.Context, itemId uint32) (model.CartItem, error)
	GetCartItemByProductId(ctx context.Context, productId string) (model.CartItem, error)
	CartItemToOrder(ctx context.Context, orderId uint32, itemsIds []uint32) error
	GetCartItemsByOrderId(ctx context.Context, orderId uint32) ([]model.CartItem, error)
}
