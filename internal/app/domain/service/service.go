package service

import (
	"bus/cart/internal/app/domain"
	"bus/cart/internal/app/domain/model"
	"bus/cart/pkg/exception"
	"bus/cart/pkg/logger"
	"context"
	"errors"
	"fmt"
)

type service struct {
	logger *logger.Logger
	r      domain.Dao
}

func (s service) GetCartItemsByOrderId(ctx context.Context, orderId uint32) ([]model.CartItem, error) {
	res, err := s.r.GetCartItemsByOrderId(ctx, orderId)
	if err != nil {
		s.logger.Error(err.Error())
		return nil, err
	}
	return res, err
}

func (s service) CartItemsToOrder(ctx context.Context, orderId uint32, itemsIds []uint32) error {
	err := s.r.CartItemToOrder(ctx, orderId, itemsIds)
	if err != nil {
		s.logger.Error(err.Error())
		return err
	}
	return nil
}

func (s service) MinceUpdateCartItem(ctx context.Context, cartItem uint32) error {
	s.logger.Debug("mince qty cart item")
	item, err := s.r.GetCartItemById(ctx, cartItem)
	if err != nil {
		return err
	}
	if item.Qty == 1 {
		s.logger.Warn("cart deleting with qty 1")
		if err = s.DeleteCartItem(ctx, cartItem); err != nil {
			return err
		}
	}
	if err = s.UpdateCartItem(ctx, item.Qty-1, cartItem); err != nil {
		return err
	}
	return nil

}

func (s service) PlusUpdateCartItem(ctx context.Context, cartItem uint32) error {
	s.logger.Debug("plus qty cart item")
	item, err := s.r.GetCartItemById(ctx, cartItem)
	if err != nil {
		return err
	}
	if err = s.UpdateCartItem(ctx, item.Qty+1, cartItem); err != nil {
		return err
	}
	return nil
}

func (s service) CreateCart(ctx context.Context, userId uint32) (uint32, error) {
	s.logger.Info("проверка наличии корзины у пользователя")
	cartId, err := s.r.CheckCart(ctx, userId)
	if cartId != 0 {
		s.logger.Error("у пользователя уже есть корзина")
		return 0, fmt.Errorf("корзина для этого пользователя %w", exception.ExistException())
	}
	s.logger.Debug("query to create cart")
	id, err := s.r.CreateCart(ctx, userId)
	if err != nil {
		s.logger.Error("при созданий корзины что то пошло не так")
		return 0, fmt.Errorf("при созданий корзины %w", exception.InternalError())
	}
	return id, nil
}

func (s service) CreateCartItem(ctx context.Context, db model.CartItem) error {
	s.logger.Debug("check cart item to exists")
	candidate, err := s.r.GetCartItemByProductId(ctx, db.ProductId)
	defer s.logger.Debug("created or updated cart item")
	if err != nil {
		return err
	}
	if candidate.Id != 0 {
		s.logger.Warn(fmt.Sprintf("already exist item with product id: %s", db.ProductId))
		err := s.UpdateCartItem(ctx, candidate.Qty, candidate.Id)
		if err != nil {
			return err
		}
	}
	err = s.r.CreateCartItem(ctx, db)
	if err != nil {
		return err
	}
	return nil
}

func (s service) UpdateCartItem(ctx context.Context, qty, cartItemId uint32) error {
	s.logger.Debug(fmt.Sprintf("query to updating cart item id: %d", cartItemId))
	err := s.r.UpdateQty(ctx, cartItemId, qty)
	if err != nil {
		return err
	}
	s.logger.Debug(fmt.Sprintf("query end to updating cart item id: %d", cartItemId))
	return nil
}

func (s service) DeleteCartItem(ctx context.Context, cartItemId uint32) error {
	s.logger.Debug(fmt.Sprintf("query to delete cart item id: %d", cartItemId))
	err := s.r.Delete(ctx, cartItemId)
	if err != nil {
		if errors.Is(err, exception.NotFoundException()) {
			return errors.New("такая вещь не существует")
		}
		return err
	}
	s.logger.Debug(fmt.Sprintf("query end to delete cart item id: %d", cartItemId))
	return nil
}

func (s service) GetOneCartItem(ctx context.Context, cartItemId uint32) (model.CartItem, error) {
	s.logger.Debug(fmt.Sprintf("query to get  item by id: %d", cartItemId))
	item, err := s.r.GetCartItemById(ctx, cartItemId)
	if err != nil {
		return model.CartItem{}, err
	}
	s.logger.Debug(fmt.Sprintf("query end to get item by id: %d", cartItemId))
	return item, nil
}

func (s service) GetCartItems(ctx context.Context, userId uint32) ([]model.CartItem, error) {
	s.logger.Debug(fmt.Sprintf("query to get cart items by user id: %d", userId))
	items, err := s.r.GetCartItems(ctx, userId)
	if err != nil {
		return items, err
	}
	s.logger.Debug(fmt.Sprintf("query end to get cart items by user id: %d", userId))
	return items, nil
}

func New(logger *logger.Logger, r domain.Dao) domain.IService {
	return service{logger, r}
}
