package service

import (
	"bus/cart/internal/app/domain/dao/mocks"
	"bus/cart/internal/app/domain/model"
	"bus/cart/pkg/exception"
	logger2 "bus/cart/pkg/logger"
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestService_CreateCart(t *testing.T) {
	emptyI := model.CartItem{
		Id: 0,
	}
	tests := []struct {
		name  string
		mock  func(cartRepo *mocks.Dao, arg model.CartItem)
		input model.CartItem
		err   error
	}{
		{
			name: "ok no exist item",
			mock: func(cartRepo *mocks.Dao, arg model.CartItem) {
				cartRepo.On("GetCartItemByProductId", context.Background(), arg.ProductId).Return(emptyI, nil)
				cartRepo.On("CreateCartItem", context.Background(), arg).Return(nil)
			},
			input: model.CartItem{
				CartId:    1,
				ProductId: "gfdgoisofds",
				Qty:       3,
			},
		},
		{
			name: "error from GetCartItemByProductId",
			mock: func(cartRepo *mocks.Dao, arg model.CartItem) {
				cartRepo.On("GetCartItemByProductId", context.Background(), arg.ProductId).Return(model.CartItem{}, fmt.Errorf("что то пошло не так"))
			},
			input: model.CartItem{
				CartId:    1,
				ProductId: "gfdgoisofds",
				Qty:       3,
			},
			err: errors.New("что то пошло не так"),
		},
		{
			name: "ok exist item",
			mock: func(cartRepo *mocks.Dao, arg model.CartItem) {
				cartRepo.On("GetCartItemByProductId", context.Background(), arg.ProductId).Return(arg, nil)
				cartRepo.On("UpdateQty", context.Background(), arg.Id, arg.Qty).Return(nil)
				cartRepo.On("CreateCartItem", context.Background(), arg).Return(nil)
			},
			input: model.CartItem{
				Id:        1,
				CartId:    1,
				ProductId: "gfdgoisofds",
				Qty:       3,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Logf("running: %s", test.name)

			ctx := context.Background()

			userRepo := mocks.NewDao(t)
			logger := logger2.New("debug")
			svc := New(logger, userRepo)
			test.mock(userRepo, test.input)

			err := svc.CreateCartItem(ctx, test.input)
			if err != nil {
				if test.err != nil {
					assert.Equal(t, test.err.Error(), err.Error())
				} else {
					t.Errorf("Expected no error, found: %s", err.Error())
				}
			}
			userRepo.AssertExpectations(t)
		})
	}
}

func TestService_DeleteCartItem(t *testing.T) {
	tests := []struct {
		name  string
		mock  func(cartRepo *mocks.Dao, arg uint32)
		input uint32
		err   error
	}{
		{
			name: "ok",
			mock: func(cartRepo *mocks.Dao, arg uint32) {
				cartRepo.On("Delete", context.Background(), arg).Return(nil)
			},
			input: 5,
		},
		{
			name: "error from store",
			mock: func(cartRepo *mocks.Dao, arg uint32) {
				cartRepo.On("Delete", context.Background(), arg).Return(fmt.Errorf("ошибка в удалений карты"))
			},
			input: 5,
			err:   errors.New("ошибка в удалений карты"),
		},
		{
			name: "error from store",
			mock: func(cartRepo *mocks.Dao, arg uint32) {
				cartRepo.On("Delete", context.Background(), arg).Return(fmt.Errorf("ошибка в удалений карты"))
			},
			input: 5,
			err:   errors.New("ошибка в удалений карты"),
		},
		{
			name: "error from store not found",
			mock: func(cartRepo *mocks.Dao, arg uint32) {
				cartRepo.On("Delete", context.Background(), arg).Return(fmt.Errorf("hz %w", exception.NotFoundException()))
			},
			input: 999,
			err:   errors.New("такая вещь не существует"),
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Logf("running: %s", test.name)

			ctx := context.Background()

			userRepo := mocks.NewDao(t)
			logger := logger2.New("debug")
			svc := New(logger, userRepo)
			test.mock(userRepo, test.input)

			err := svc.DeleteCartItem(ctx, test.input)
			if err != nil {
				if test.err != nil {
					assert.Equal(t, test.err.Error(), err.Error())
				} else {
					t.Errorf("Expected no error, found: %s", err.Error())
				}
			}
			userRepo.AssertExpectations(t)
		})
	}
}
func TestService_GetCartItems(t *testing.T) {

}
