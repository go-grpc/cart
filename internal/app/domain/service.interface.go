package domain

import (
	"bus/cart/internal/app/domain/model"
	"context"
)

//go:generate mockery --dir . --name IService --output service/mocks
type IService interface {
	CreateCart(ctx context.Context, userId uint32) (uint32, error)
	CreateCartItem(ctx context.Context, db model.CartItem) error
	UpdateCartItem(ctx context.Context, qty, cartItemId uint32) error
	DeleteCartItem(ctx context.Context, cartItemId uint32) error
	GetOneCartItem(ctx context.Context, cartItemId uint32) (model.CartItem, error)
	GetCartItems(ctx context.Context, cartId uint32) ([]model.CartItem, error)
	MinceUpdateCartItem(ctx context.Context, cartItem uint32) error
	PlusUpdateCartItem(ctx context.Context, cartItem uint32) error
	GetCartItemsByOrderId(ctx context.Context, orderId uint32) ([]model.CartItem, error)
	CartItemsToOrder(ctx context.Context, orderId uint32, itemsIds []uint32) error
}
