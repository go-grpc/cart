package model

import "bus/cart/internal/app/adapter/grpc/proto"

type Cart struct {
	Id     uint32 `json:"id"`
	UserId uint32 `json:"user_id"`
}

type CartItem struct {
	Id        uint32 `json:"id"`
	CartId    uint32 `json:"cart_id"`
	ProductId string `json:"product_id"`
	Qty       uint32 `json:"qty"`
	OrderId   uint32 `json:"order_id"`
}

func (c CartItem) ToGRPC() *proto.CartItem {
	return &proto.CartItem{
		Id:        c.Id,
		CartId:    c.CartId,
		ProductId: c.ProductId,
		Qty:       c.Qty,
		OrderId:   c.OrderId,
	}
}

type CartDB struct {
	Id     uint32 `pg:"id"`
	UserId uint32 `pg:"user_id"`
}
type CartItemDB struct {
	CartId    uint32 `pg:"cart_id"`
	ProductId string `pg:"product_id"`
	Qty       uint32 `pg:"qty"`
}

func (c *CartItemDB) toWEB() CartItem {
	return CartItem{
		CartId:    c.CartId,
		ProductId: c.ProductId,
		Qty:       c.Qty,
	}
}

func (c *CartItemDB) tableName() string {
	return "cart_items"
}

func (c *CartDB) toWEB() Cart {
	return Cart{
		Id:     c.Id,
		UserId: c.UserId,
	}
}

func (c *CartDB) tableName() string {
	return "cart"
}

type CreateCart struct {
	UserId uint32 `json:"user_id"`
}
type CreateCartItemDto struct {
	UserId uint32 `json:"user_id"`
}

type DeleteCartItem struct {
	CartItemId uint32 `json:"cart_item_id"`
}

type UpdateCartItem struct {
	CartItemId uint32 `json:"cart_item_id"`
}

type UpdateCartItemToOrderDto struct {
	OrderId     uint32   `json:"order_id"`
	CartItemIds []uint32 `json:"cart_item_ids"`
}
