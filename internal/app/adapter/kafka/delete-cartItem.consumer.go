package kafka

import (
	"bus/cart/internal/app/domain/model"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p CartMessageProcessor) deleteCartItem(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Info("starting delete cart item process")
	var msg model.DeleteCartItem
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in unmarshal in deleting cart item")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return p.svc.DeleteCartItem(ctx, msg.CartItemId)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in delete cart item")
		return
	}
	p.logger.Debug("success process delete cart item")
}
