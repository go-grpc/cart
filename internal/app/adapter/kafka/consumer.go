package kafka

import (
	"bus/cart/internal/app/domain/model"
	"bus/cart/pkg/logger"
	"context"
	"fmt"
	"github.com/segmentio/kafka-go"
	"os"
)

type service interface {
	CreateCartItem(ctx context.Context, db model.CartItem) error
	DeleteCartItem(ctx context.Context, cartItemId uint32) error
	MinceUpdateCartItem(ctx context.Context, cartItem uint32) error
	PlusUpdateCartItem(ctx context.Context, cartItem uint32) error
	CartItemsToOrder(ctx context.Context, orderId uint32, itemsIds []uint32) error
}

type CartMessageProcessor struct {
	logger *logger.Logger
	svc    service
}

func New(logger *logger.Logger, svc service) CartMessageProcessor {
	return CartMessageProcessor{logger, svc}
}

func (p CartMessageProcessor) ProcessMessages(ctx context.Context, r *kafka.Reader, workerID int) {
	p.logger.Info("starting process messages")

	for {
		select {
		case <-ctx.Done():
			return
		default:

		}

		m, err := r.FetchMessage(ctx)
		fmt.Println(err)
		if err != nil {
			p.logger.Error(err, workerID)
			continue
		}
		switch m.Topic {
		case os.Getenv("CREATE_CARTITEM_EVENT"):
			p.createCartItem(ctx, r, m)
		case os.Getenv("DELETE_CARTITEM_EVENT"):
			p.deleteCartItem(ctx, r, m)
		case os.Getenv("MINCE_QTY_EVENT"):
			p.minceUpdateCartItem(ctx, r, m)
		case os.Getenv("PLUS_QTY_EVENT"):
			p.plusUpdateCartItem(ctx, r, m)
		case os.Getenv("ADD_CARTITEM_TO_ORDER_EVENT"):
			p.cartItemsToOrder(ctx, r, m)
		default:
			continue
		}

	}
}
