package kafka

import (
	"bus/cart/internal/app/domain/model"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p CartMessageProcessor) createCartItem(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Info("starting create cart item process")
	var msg model.CartItem
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in unmarshal")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return p.svc.CreateCartItem(ctx, msg)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in creating cart item")
		return
	}
	p.logger.Debug("success process creating cart item")
}
