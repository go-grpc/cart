package kafka

import (
	"bus/cart/internal/app/domain/model"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p CartMessageProcessor) cartItemsToOrder(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Info("starting update cart item to order process")

	var msg model.UpdateCartItemToOrderDto

	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in update cart item to order")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return p.svc.CartItemsToOrder(ctx, msg.OrderId, msg.CartItemIds)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in update cart item to order")
		return
	}
	p.logger.Debug("success process update cart item to order")
}
