package kafka

import (
	"bus/cart/internal/app/domain/model"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p CartMessageProcessor) plusUpdateCartItem(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Info("starting plus cart item qty process")
	var msg model.UpdateCartItem
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in unmarshal in plus cart item qty")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return p.svc.PlusUpdateCartItem(ctx, msg.CartItemId)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in plus cart item qty")
		return
	}
	p.logger.Debug("success process plus cart item qty")
}
