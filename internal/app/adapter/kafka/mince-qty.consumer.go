package kafka

import (
	"bus/cart/internal/app/domain/model"
	"context"
	"encoding/json"
	"github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
)

func (p CartMessageProcessor) minceUpdateCartItem(ctx context.Context, r *kafka.Reader, m kafka.Message) {
	p.logger.Info("starting mince cart item qty process")
	var msg model.UpdateCartItem
	if err := json.Unmarshal(m.Value, &msg); err != nil {
		p.logger.Error("error in unmarshal in mince cart item qty")
		return
	}

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return p.svc.MinceUpdateCartItem(ctx, msg.CartItemId)
	})
	err := g.Wait()
	if err != nil {
		p.logger.Error("error in mince cart item qty")
		return
	}
	p.logger.Debug("success process mince cart item qty")
}
