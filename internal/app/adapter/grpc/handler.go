package grpc

import (
	"bus/cart/internal/app/adapter/grpc/proto"
	"bus/cart/internal/app/domain/model"
	"bus/cart/pkg/exception"
	"bus/cart/pkg/logger"
	"context"
	"errors"
	"google.golang.org/grpc"
)

type service interface {
	GetOneCartItem(ctx context.Context, cartItemId uint32) (model.CartItem, error)
	GetCartItems(ctx context.Context, cartId uint32) ([]model.CartItem, error)
	GetCartItemsByOrderId(ctx context.Context, orderId uint32) ([]model.CartItem, error)
}
type server struct {
	svc    service
	logger *logger.Logger
}

func (s server) CreateCart(ctx context.Context, request *proto.CreateCartRequest) (*proto.CreateCartResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (s server) GetOneCartItem(ctx context.Context, request *proto.GetCartItemRequest) (*proto.GetCartItemResponse, error) {
	s.logger.Info("request service from grpc get one cartItem")
	response := &proto.Response{
		Status: 200,
		Error:  "",
	}
	item, err := s.svc.GetOneCartItem(ctx, request.CartItemId)
	if err != nil {
		return &proto.GetCartItemResponse{Response: errorHandler(err, response)}, nil
	}
	return &proto.GetCartItemResponse{Item: item.ToGRPC(), Response: response}, nil
}

func (s server) GetCartItems(ctx context.Context, request *proto.GetCartItemsRequest) (*proto.GetCartItemsResponse, error) {
	s.logger.Info("request service from grpc get  cartItems")
	response := &proto.Response{
		Status: 200,
		Error:  "",
	}
	items, err := s.svc.GetCartItems(ctx, request.GetUserId())
	if err != nil {
		return &proto.GetCartItemsResponse{Response: errorHandler(err, response)}, nil
	}
	var cartItems []*proto.CartItem
	for _, item := range items {
		cartItems = append(cartItems, item.ToGRPC())
	}
	s.logger.Info("end request service from grpc get  cartItems")
	return &proto.GetCartItemsResponse{Items: cartItems, Response: errorHandler(err, response)}, nil
}

func (s server) GetCartItemsByOrderId(ctx context.Context, request *proto.GetCartItemsByOrderIdRequest) (*proto.GetCartItemsResponse, error) {
	s.logger.Info("request service from grpc get  cartItems by orderId")
	response := &proto.Response{
		Status: 200,
		Error:  "",
	}
	items, err := s.svc.GetCartItemsByOrderId(ctx, request.GetOrderId())
	if err != nil {
		return &proto.GetCartItemsResponse{Response: errorHandler(err, response)}, nil
	}
	var cartItems []*proto.CartItem
	for _, item := range items {
		cartItems = append(cartItems, item.ToGRPC())
	}
	s.logger.Info("end request service from grpc get cartItems by orderId")
	return &proto.GetCartItemsResponse{Items: cartItems, Response: errorHandler(err, response)}, nil

}

func RegisterGrpc(gServer *grpc.Server, s service, logger *logger.Logger) {
	server := server{s, logger}
	proto.RegisterAuthServiceServer(gServer, &server)
}

func errorHandler(err error, response *proto.Response) *proto.Response {
	if errors.Is(err, exception.NotFoundException()) {
		response.Status = 404
		response.Error = err.Error()
		return response

	}
	if errors.Is(err, exception.DecodeException()) {
		response.Status = 500
		response.Error = err.Error()
		return response
	}
	if errors.Is(err, exception.ExistException()) {
		response.Status = 400
		response.Error = err.Error()
		return response
	}
	response.Status = 500
	response.Error = err.Error()
	return response
}
