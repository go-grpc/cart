package app

import (
	grpc2 "bus/cart/internal/app/adapter/grpc"
	"bus/cart/internal/app/adapter/kafka"
	"bus/cart/internal/app/domain"
	"bus/cart/internal/app/domain/dao"
	"bus/cart/internal/app/domain/service"
	"bus/cart/pkg/client/postgresql"
	"bus/cart/pkg/logger"
	"context"
	"fmt"
	"github.com/joho/godotenv"
	kafka2 "github.com/segmentio/kafka-go"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net"
	"os"
)

type App struct {
	pg         postgresql.Client
	ctx        context.Context
	dao        domain.Dao
	logger     *logger.Logger
	service    domain.IService
	processor  kafka.CartMessageProcessor
	reader     *kafka2.Reader
	listener   net.Listener
	grpcServer *grpc.Server
	kafkaConn  *kafka2.Conn
}

func New() (*App, error) {
	a := &App{}
	a.ctx = context.Background()
	a.logger = logger.New(os.Getenv("LOGGER_LEVEL"))
	err := godotenv.Load()
	if err != nil {
		a.logger.Fatal("Error loading .env file")
		return nil, fmt.Errorf("exception loading .env file")
	}
	k, err := kafka2.DialContext(context.Background(), "tcp", "localhost:9092")
	if err != nil {
		//a.logger.Fatal(err)
	}
	a.kafkaConn = k
	a.grpcServer = grpc.NewServer()
	pgClient, err := postgresql.New(
		a.ctx,
		5,
		os.Getenv("POSTGRES_USER"),
		os.Getenv("postgres_password"),
		os.Getenv("postgres_host"),
		os.Getenv("POSTGRES_PORT"),
		os.Getenv("POSTGRES_DB"))
	a.pg = pgClient
	if err != nil {
		a.logger.Fatal("can not connect to postgresql")
		return nil, fmt.Errorf("can not connect to postgresql")
	}
	a.reader = kafka2.NewReader(kafka2.ReaderConfig{
		Brokers: []string{"localhost:9092"},
		GroupID: "my-group",
		GroupTopics: []string{
			os.Getenv("ADD_CARTITEM_TO_ORDER_EVENT"),
			os.Getenv("CREATE_CARTITEM_EVENT"),
			os.Getenv("DELETE_CARTITEM_EVENT"),
			os.Getenv("MINCE_QTY_EVENT"),
			os.Getenv("PLUS_QTY_EVENT"),
		},
	})
	a.dao = dao.New(a.pg, a.logger)
	a.service = service.New(a.logger, a.dao)
	grpc2.RegisterGrpc(a.grpcServer, a.service, a.logger)
	a.processor = kafka.New(a.logger, a.service)
	return a, nil
}

func (a *App) Run() error {
	go a.processor.ProcessMessages(a.ctx, a.reader, 1)
	g, _ := errgroup.WithContext(a.ctx)
	g.Go(func() error {
		return a.grpcServer.Serve(a.listener)
	})
	err := g.Wait()
	if err != nil {
		a.logger.Error(err)
		return err
	}
	return nil
}
